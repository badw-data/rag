# Daten des Repertorium Academicum Germanicum (RAG)

Siehe <https://rag-online.org>, <https://rag-online.org/impressum/impressum>.

Die Urdaten befinden sich im Verzeichnis `/Daten` (<https://gitlab.lrz.de/badw-data/rag/-/tree/master/Daten>).

Ansichten im OpenDocument-Format (ODS), die mit LibreOffice-Calc oder anderen Tabellenkalkulationsprogrammen gelesen werden können, liegen im Verzeichnis `/Daten-ods` (<https://gitlab.lrz.de/badw-data/rag/-/tree/master/Daten-ods>).

Die Daten dürfen unter den Bedingungen der Lizenz [Creative Commons Namensnennung 4.0 International (CC-BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de) heruntergeladen und genutzt werden.

Die Daten in diesem Repositorium befinden sich auf dem Stand von Ende 2019, d.h. des Zeitpunktes des Abschlusses des "Repertorium Academicum Germanicum" als Projekt der Bayerischen Akademie der Wissenschaften.

Um die Daten zu referenzieren sollte am Besten nicht die gitlab-URL verwendet werden, sondern eine der folgenden Internetadressen:

- <https://daten.badw.de/rag/tree/v2019.12.0>  (Permanente, zitierbare Adresse einer bestimmten Version des Datenbestandes)
- <https://daten.badw.de/rag> (Permanente Internetadresse des Daten-Repositoriums und zugleich der jeweils jüngsten Version)
